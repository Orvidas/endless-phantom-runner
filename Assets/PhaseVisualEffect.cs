﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseVisualEffect : MonoBehaviour {
    [SerializeField]
    private LayerMask insideLayerMask;
    private Phase playerPhase;
    private SpriteRenderer spriteRenderer;
    private Texture2D originalPhaseTexture;
    private bool phasingThrough;
    private BoxCollider2D textureCollider;
    private RaycastHit2D bottomRay, rightRay, topRay, leftRay;
    private float shortRayLength, longRayLength;
    private Rigidbody2D playerBody;

    void Start() {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        playerBody = player.GetComponent<Rigidbody2D>();
        playerPhase = player.GetComponent<Phase>();

        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, 1f);

        textureCollider = GetComponent<BoxCollider2D>();

        shortRayLength = textureCollider.bounds.size.x;
        longRayLength = textureCollider.bounds.size.y;

        originalPhaseTexture = GetComponent<SpriteRenderer>().sprite.texture;
        phasingThrough = false;

       // Debug.Log(originalPhaseTexture.texelSize);

        UpdateRaycastBox();
        UpdateEffectTexture();
    }

    void FixedUpdate() {
        if(playerPhase.GetCurrentPhaseState())
            UpdateRaycastBox();
    }

    private void UpdateRaycastBox() {
        bottomRay = Physics2D.Linecast(textureCollider.bounds.min, textureCollider.bounds.center + new Vector3(textureCollider.bounds.extents.x, textureCollider.bounds.extents.y * -1), insideLayerMask);
        rightRay = Physics2D.Linecast(textureCollider.bounds.center + new Vector3(textureCollider.bounds.extents.x, textureCollider.bounds.extents.y * -1), textureCollider.bounds.max, insideLayerMask);
        topRay = Physics2D.Linecast(textureCollider.bounds.max, textureCollider.bounds.center + new Vector3(textureCollider.bounds.extents.x * -1, textureCollider.bounds.extents.y), insideLayerMask);
        leftRay = Physics2D.Linecast(textureCollider.bounds.center + new Vector3(textureCollider.bounds.extents.x * -1, textureCollider.bounds.extents.y), textureCollider.bounds.min, insideLayerMask);
    }

    void OnTriggerStay2D(Collider2D other) {
        if (playerPhase.GetCurrentPhaseState() && AnyRayHit()) {
            phasingThrough = true;
            UpdateEffectTexture();
        }
    }

    private bool AnyRayHit() {
        return bottomRay || rightRay || leftRay; //|| topRay || leftRay;
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Inside") && phasingThrough) {
            phasingThrough = false;
            UpdateEffectTexture();
        }
    }

    private void UpdateEffectTexture() {
        int[] ratioOfPixelsToShow = CalculateWhichPixelsToShow();
        Texture2D updatedTexture = GetNewTexture(originalPhaseTexture, ratioOfPixelsToShow[0], ratioOfPixelsToShow[1]);

        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        string tempName = sr.sprite.name;
        sr.sprite = Sprite.Create(updatedTexture, sr.sprite.rect, new Vector2(0.5f, 0.5f), sr.sprite.pixelsPerUnit);
        sr.sprite.name = tempName;

        sr.material.mainTexture = updatedTexture;
        sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 1f);
        //sr.material.shader = Shader.Find("Sprites/Transparent Unlit");
    }

    private int[] CalculateWhichPixelsToShow() {
        float bottomRatioCovered = 1 - (bottomRay.distance / shortRayLength);
        int xPixels = (int) Mathf.Ceil(bottomRatioCovered * originalPhaseTexture.width);

        float rightRatioCovered = (playerBody.velocity.y > 0) ? 1 - (rightRay.distance / longRayLength) : 1 - (leftRay.distance / longRayLength);
        int yPixels = (int) Mathf.Ceil(rightRatioCovered * originalPhaseTexture.height);
        
        return new int[] { xPixels, yPixels };
    }

    private Texture2D GetNewTexture(Texture2D originalTexture, int xPixelsToShow, int yPixelsToShow) {
        //Create a new Texture2D, which will be the copy.
        Texture2D copiedTexture = new Texture2D(originalTexture.width, originalTexture.height);
        //Choose your filtermode and wrapmode here.
        copiedTexture.filterMode = FilterMode.Point;
        copiedTexture.wrapMode = TextureWrapMode.Clamp;

        int x = 0, y = 0;
        foreach (Color pixel in originalTexture.GetPixels()) {
            if (x == copiedTexture.width) {
                y++;
                x = 0;
            }

            if (y == copiedTexture.height)
                break;

            //Turn on alpha here
            if (playerPhase.GetCurrentPhaseState() && phasingThrough && (x >= copiedTexture.width - xPixelsToShow && y >= copiedTexture.height - yPixelsToShow)) {
                copiedTexture.SetPixel(x, y, new Color(pixel.r, pixel.g, pixel.b, 0.3f));
               // Debug.Log(xPixelsToShow + " " + yPixelsToShow);
            }
            else {
                copiedTexture.SetPixel(x, y, new Color(pixel.r, pixel.g, pixel.b, 0f));
            }

            x++;
        }

        //Name the texture, if you want.
        copiedTexture.name = ("Test_SpriteSheet");

        //This finalizes it. If you want to edit it still, do it before you finish with .Apply(). Do NOT expect to edit the image after you have applied. It did NOT work for me to edit it after this function.
        copiedTexture.Apply();

        //Return the variable, so you have it to assign to a permanent variable and so you can use it.
        return copiedTexture;
    }

    bool isLayerInMask(int layer) {
        return insideLayerMask == (insideLayerMask | (1 << layer));
    }
}
