﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorTest : MonoBehaviour {
    [Range(0,3)]
    public int xPoints = 0;
    [Range(0, 3)] 
    public int yPoints = 0;

    private int lastXPoint;
    private int lastYPoint;
    private Texture2D originalSpriteTexture;
    // Use this for initialization
    void Start () {
        originalSpriteTexture = gameObject.GetComponent<SpriteRenderer>().sprite.texture;

        for (int y = 0; y < originalSpriteTexture.height; y++)
            for (int x = 0; x < originalSpriteTexture.width; x++)
                originalSpriteTexture.SetPixel(x, y, new Color(94, 60, 60, 0));

        UpdateCharacterTexture();
        lastYPoint = xPoints;
        lastXPoint = yPoints;
    }
	
	// Update is called once per frame
	void Update () {
		if(xPoints != lastXPoint || yPoints != lastYPoint) {
            UpdateCharacterTexture();
        }
	}

    public void UpdateCharacterTexture() {
        //This calls the copy texture function, and copies it. The variable characterTextures2D is a Texture2D which is now the returned newly copied Texture2D.
        Texture2D characterTexture2D = CopyTexture2D(originalSpriteTexture);
        //Get your SpriteRenderer, get the name of the old sprite,  create a new sprite, name the sprite the old name, and then update the material. If you have multiple sprites, you will want to do this in a loop- which I will post later in another post.
        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        string tempName = sr.sprite.name;
        sr.sprite = Sprite.Create(characterTexture2D, sr.sprite.rect, new Vector2(0.5f, 0.5f), 4f);
        sr.sprite.name = tempName;

        sr.material.mainTexture = characterTexture2D;
        //sr.material.shader = Shader.Find("Sprites/Transparent Unlit");
    }

    public Texture2D CopyTexture2D(Texture2D originalTexture) {
        //Create a new Texture2D, which will be the copy.
        Texture2D copiedTexture = new Texture2D(originalTexture.width, originalTexture.height);
        //Choose your filtermode and wrapmode here.
        copiedTexture.filterMode = FilterMode.Point;
        copiedTexture.wrapMode = TextureWrapMode.Clamp;

        int y = 0;
        while (y < copiedTexture.height) {
            int x = 0;
            while (x < copiedTexture.width) {
                //INSERT YOUR LOGIC HERE
                if (x == xPoints || y == yPoints) {
                    //This line of code and if statement, turn Green pixels into Red pixels.
                    copiedTexture.SetPixel(x, y, new Color(0,0,1,1));
                }
                else {
                    //This line of code is REQUIRED. Do NOT delete it. This is what copies the image as it was, without any change.
                    copiedTexture.SetPixel(x, y, originalTexture.GetPixel(x, y));
                }
                ++x;
            }
            ++y;
        }
        //Name the texture, if you want.
        copiedTexture.name = ("Test_SpriteSheet");

        //This finalizes it. If you want to edit it still, do it before you finish with .Apply(). Do NOT expect to edit the image after you have applied. It did NOT work for me to edit it after this function.
        copiedTexture.Apply();

        //Return the variable, so you have it to assign to a permanent variable and so you can use it.
        return copiedTexture;
    }
}
