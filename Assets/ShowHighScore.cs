﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowHighScore : MonoBehaviour {
    [SerializeField]
    private Text highScoreText;
    private int highScoreValue;

	// Use this for initialization
	void Start () {
        highScoreValue = PlayerPrefs.GetInt("TempHighscore");
        highScoreText = GetComponent<Text>();

        highScoreText.text = "Current High Score: " + highScoreValue;
	}
}
