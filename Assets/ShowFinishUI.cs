﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowFinishUI : MonoBehaviour {
    [SerializeField] private GameObject jumpButton;
    [SerializeField] private GameObject phaseButton;
    [SerializeField] private GameObject finishPanel;

    public void ShowFinish() {
        jumpButton.SetActive(false);
        phaseButton.SetActive(false);
        finishPanel.SetActive(true);
    }
}
