﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroll : MonoBehaviour {
    private SpriteRenderer backgroundRenderer;
    private Transform backgroundTransform;

    void Start () {
        backgroundRenderer = GetComponent<SpriteRenderer>();
        backgroundTransform = GetComponent<Transform>();
    }

    void OnBecameInvisible() {
        Vector3 currentPos = backgroundTransform.position;
        var backgroundSize = backgroundRenderer.bounds.size;
        var distanceToMove = backgroundSize.x * 2;

        //new position
        backgroundTransform.position = new Vector3(currentPos.x + distanceToMove, backgroundTransform.position.y, backgroundTransform.position.z);
        //Debug.Log("PPU = " + backgroundRenderer.sprite.pixelsPerUnit + " and Size = " + backgroundRenderer.size);
    }
}
