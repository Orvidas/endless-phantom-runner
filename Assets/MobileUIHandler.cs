﻿using UnityEngine;

public class MobileUIHandler : MonoBehaviour {

	void Awake () {
#if UNITY_IOS || UNITY_ANDROID
        gameObject.SetActive(true);
#else
        gameObject.SetActive(false);
#endif
    }
}
