﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour {
    [SerializeField]
    private GameObject jumpButton;
    [SerializeField]
    private GameObject phaseButton;
    [SerializeField]
    private GameObject finishPanel;
    private bool gameOver;

    // Use this for initialization
    void Start () {
        gameOver = false;
        gameObject.SetActive(false);
	}
	
    public void ShowGameOverScreen() {
        gameOver = true;
        jumpButton.SetActive(false);
        phaseButton.SetActive(false);
        finishPanel.SetActive(true);
    }

    public void PlayAgain() {
        if (gameOver) 
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu() {
        int mainMenuIndex = 0;

        if (gameOver)
            SceneManager.LoadScene(mainMenuIndex);
    }
}
