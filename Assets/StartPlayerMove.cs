﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class StartPlayerMove : MonoBehaviour {
    private AutoMove movePlayer;
    private Button jumpButton;
    private bool activated = false;

    void Start() {
        movePlayer = GameObject.Find("InputManager").GetComponent<AutoMove>();
        jumpButton = GameObject.Find("Jump").GetComponent<Button>();
        
        movePlayer.enabled = false;
        jumpButton.interactable = true;
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player" && !activated) {
            activated = true;
            StartCoroutine(StartEscapeCutscene());
        }
    }
    
    IEnumerator StartEscapeCutscene() {
        jumpButton.interactable = false;
        yield return new WaitForSeconds(2);
        movePlayer.enabled = true;
        jumpButton.interactable = true;
    }
}
