﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonActive : EventTrigger {
    private JumpTouchInput jumpInput;
    private Button jumpButton;

	void Start () {
        jumpInput = GameObject.Find("InputManager").GetComponent<JumpTouchInput>();
        jumpButton = GetComponent<Button>();
    }

    public override void OnPointerDown(PointerEventData data) {
        if (jumpButton.interactable)
            jumpInput.PlayerJump();
    }

    public override void OnPointerUp(PointerEventData data) {
        if (jumpButton.interactable)
            jumpInput.CancelPlayerJump();
    }
}
