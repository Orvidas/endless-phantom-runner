﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerSpawn : MonoBehaviour {
    [SerializeField]
    private static Vector3 respawnPoint;
    [SerializeField]
    private static int currentSceneIndex = -1;
    
	void Awake () {
        if (currentSceneIndex != SceneManager.GetActiveScene().buildIndex) {
            respawnPoint = GetStartingPoint();
            currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        }
        transform.position = respawnPoint;
	}

    private Vector3 GetStartingPoint() {
        return GameObject.Find("StartingPoint").transform.position;
    }
	
    //Gets new checkpoint
	void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Respawn")
            respawnPoint = other.gameObject.transform.position;
    }
}
