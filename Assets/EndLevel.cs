﻿using UnityEngine;

public class EndLevel : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D coll) {
        if(coll.gameObject.tag == "Player") {
            GameObject inputManager = GameObject.Find("InputManager");
            inputManager.GetComponent<AutoMove>().enabled = false;
            coll.gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;

            GameObject canvas = GameObject.Find("Canvas");
            canvas.GetComponent<ShowFinishUI>().ShowFinish();

            CameraFollow camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFollow>();
            if (camera.GetCameraCanPassPlayerStatus())
                camera.ToggleCameraCanPassPlayer();
        }
    }
}
