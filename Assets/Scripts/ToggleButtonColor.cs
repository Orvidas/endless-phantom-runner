﻿using UnityEngine.UI;
using UnityEngine;

public class ToggleButtonColor : MonoBehaviour {
    private Image image;
    private Color normalColor;
    private Color selectedColor;
    private bool currentlyActive;

	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();

        normalColor = GetComponent<Button>().colors.normalColor;
        selectedColor = GetComponent<Button>().colors.pressedColor;
        normalColor.a = 1f;
        selectedColor.a = 1f;

        currentlyActive = false;
    }

    public void ToggleColor() {
        currentlyActive = !currentlyActive;
        image.color = (currentlyActive) ? selectedColor : normalColor;
    }
}
