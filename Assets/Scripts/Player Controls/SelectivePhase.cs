﻿using UnityEngine;

public class SelectivePhase : MonoBehaviour, Phase {
    private SpriteRenderer[] allSprites; 
    private Color[] allTransparencyEffects; 
    private bool currentlyPhased = false;
    private int playerLayer;
    private int wallLayer;
    private int floorLayer;
    [SerializeField]
    private Collider2D floorCollider;

	// Use this for initialization
	void Start () {
        playerLayer = LayerMask.NameToLayer("Player");
        wallLayer = LayerMask.NameToLayer("Wall");
        floorLayer = LayerMask.NameToLayer("Floor");
        Physics2D.IgnoreLayerCollision(playerLayer, wallLayer, false);
        
        allSprites = GetComponentsInChildren<SpriteRenderer>();
        allTransparencyEffects = new Color[allSprites.Length];
        for (int i = 0; i < allSprites.Length; i++) {
            allTransparencyEffects[i] = allSprites[i].color;
        }
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.layer == floorLayer)
            floorCollider = other.gameObject.GetComponent<Collider2D>();
    }

    private void OnTriggerExit2D(Collider2D other) {
        other.isTrigger = false;
    }
    
    public void TogglePhase() {
        currentlyPhased = !currentlyPhased;
        Physics2D.IgnoreLayerCollision(playerLayer, wallLayer, currentlyPhased);
        ToggleTransparency();
    }
    
    public void DropThroughFloor() {
        if(currentlyPhased)
            floorCollider.isTrigger = true;
    }

    public bool GetCurrentPhaseState() {
        return currentlyPhased;
    }

    private void ToggleTransparency() {
        for (int i = 0; i < allTransparencyEffects.Length; i++) {
            allTransparencyEffects[i].a = (currentlyPhased) ? 0.64f : 1.0f;
            allSprites[i].color = allTransparencyEffects[i];
        }
    }
}
