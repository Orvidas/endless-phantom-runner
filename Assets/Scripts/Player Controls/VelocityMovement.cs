﻿using UnityEngine;

public class VelocityMovement : MonoBehaviour, Movement {
    private Rigidbody2D player;
    private float moveSpeed = 15f;

    void Start () {
        player = GetComponent<Rigidbody2D>();
	}
	
    public float GetMoveSpeed() {
        return moveSpeed;
    }
    
    public void Move(float moveAxis) {
        float moveDirection = (moveAxis == 0) ? 0 : Mathf.Abs(moveAxis) / moveAxis;

        player.velocity = new Vector2(moveSpeed * moveDirection, player.velocity.y);
    }
}
