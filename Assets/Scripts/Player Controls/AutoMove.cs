﻿using UnityEngine;

public class AutoMove : MonoBehaviour {
    private Movement playerMovement;
	// Use this for initialization
	void Start () {
        playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<Movement>();
	}
	
	// Update is called once per frame
	void Update () {
        playerMovement.Move(1);
	}
}
