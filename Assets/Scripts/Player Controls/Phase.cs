﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
interface Phase {
    void TogglePhase();

    void DropThroughFloor();

    bool GetCurrentPhaseState();
}
