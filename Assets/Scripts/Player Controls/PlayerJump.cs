﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour {
    private Rigidbody2D player;
    private AudioSource jumpClip;
	// Use this for initialization
	void Start () {
        player = GetComponent<Rigidbody2D>();
        jumpClip = GetComponent<AudioSource>();
    }

    public void Jump() {
        float jumpForce = 800f;
        player.AddForce(new Vector2(0, jumpForce));
        jumpClip.Play();
    }

    public void CancelJump() {
        if (player.velocity.y > 0)
            player.velocity = new Vector2(player.velocity.x, 0);
    }
}
