﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbsolutePhase : MonoBehaviour, Phase {
    private bool currentlyPhased;
    [SerializeField] private bool canTogglePhase;
    private int playerLayer;
    private int floorLayer;
    private int wallLayer;

    private SpriteRenderer spriteRenderer;
    private Color transparencyEffect;
    private bool bufferTogglePhase;

    void Start() {
        currentlyPhased = false;
        canTogglePhase = true;

        spriteRenderer = GetComponent<SpriteRenderer>();
        transparencyEffect = spriteRenderer.color;

        playerLayer = LayerMask.NameToLayer("Player");
        floorLayer = LayerMask.NameToLayer("Floor");
        wallLayer = LayerMask.NameToLayer("Wall");

        Physics2D.IgnoreLayerCollision(playerLayer, floorLayer, false);
        Physics2D.IgnoreLayerCollision(playerLayer, wallLayer, false);
    }

    void FixedUpdate() {
        //if (!canTogglePhase) Debug.Log("Cannot toggle the phase right now");
        if(bufferTogglePhase && canTogglePhase) {
            TogglePhase();
            bufferTogglePhase = false;
        }
    }
    
    public void DropThroughFloor() {
        throw new NotImplementedException();
    }

    public bool GetCurrentPhaseState() {
        return currentlyPhased;
    }

    void OnTriggerStay2D(Collider2D other) {
        canTogglePhase = (other.gameObject.layer == LayerMask.NameToLayer("Inside") && currentlyPhased) ? false : true;
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Inside"))
            canTogglePhase = true;
    }

    public void TogglePhase() {
        if (canTogglePhase) {
            currentlyPhased = !currentlyPhased;
            Physics2D.IgnoreLayerCollision(playerLayer, floorLayer, currentlyPhased);
            Physics2D.IgnoreLayerCollision(playerLayer, wallLayer, currentlyPhased);

            ToggleTransparency();
        }
        else {
            bufferTogglePhase = true;
        }
    }

    private void ToggleTransparency() {
        transparencyEffect.a = (currentlyPhased) ? 0.64f : 1.0f;

        spriteRenderer.color = transparencyEffect;
    }
}
