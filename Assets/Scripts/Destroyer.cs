﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Destroyer : MonoBehaviour {
    private Score score;
    private GameObject gameOverPanel;

    void Awake() {
        score = GameObject.FindGameObjectWithTag("Score").GetComponent<Score>();
        gameOverPanel = GameObject.FindGameObjectWithTag("Game Over");
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            score.SetAutoIncreaseScore(false);
            score.UpdateHighScore();
            StopMoving();
            //StopCamera();
            Invoke("StopCamera", .1f);
            DestroySpawners();
            gameOverPanel.SetActive(true);
            gameOverPanel.GetComponent<GameOverManager>().ShowGameOverScreen();
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.tag != "Player" && other.gameObject.layer != LayerMask.NameToLayer("UI")) {
            Destroy(other.gameObject);
        }
    }

    private void StopCamera() {
        CameraFollow camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraFollow>();
        if (!camera.IsCameraStopped())
            camera.ToggleMoveCamera();
    }

    private static void StopMoving() {
        GameObject inputManager = GameObject.Find("InputManager");
        inputManager.GetComponent<AutoMove>().enabled = false;

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }

    private static void DestroySpawners() {
        foreach (GameObject spawner in GameObject.FindGameObjectsWithTag("Spawner")) {
            Destroy(spawner);
        }
    }
}
