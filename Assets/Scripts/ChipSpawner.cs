﻿using UnityEngine;

public class ChipSpawner : MonoBehaviour, Spawner {
    public GameObject memoryChip;
    public int memoryChipValue = 5;
    public Color memoryChipColor;
    public float minSpawnTime = 2f;
    public float maxSpawnTime = 3.5f;

    private GameObject otherMemoryChip;
    private float currentWaitTimer;
    private float endOfWaitTimer;

    // Use this for initialization
    void Start () {
        otherMemoryChip = memoryChip;
        otherMemoryChip.GetComponent<SpriteRenderer>().color = memoryChipColor;
        otherMemoryChip.GetComponent<MemoryChip>().changeScoreBy = memoryChipValue;
        StartNewTimers();
	}
	
	// Update is called once per frame
	void Update () {
        currentWaitTimer += Time.deltaTime;
        if (currentWaitTimer >= endOfWaitTimer) {
            StartNewTimers();
            SpawnObjects();
        }
    }

    public void StartNewTimers() {
        currentWaitTimer = 0;
        endOfWaitTimer = Random.Range(minSpawnTime, maxSpawnTime);
    }

    public void SpawnObjects() {
        Vector3 position = new Vector3(transform.position.x, transform.position.y + 2, 0);
        Instantiate(otherMemoryChip, position, transform.rotation);
    }
}
