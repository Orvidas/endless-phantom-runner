﻿using UnityEngine;

public class JumpTouchInput : MonoBehaviour {
    private GroundChecker groundCheck;

    [SerializeField]
    private bool grounded;
    private float timeOffGround;
    private bool performedSingleJump;
    private bool performedDoubleJump;
    private GameObject player;
    private PlayerJump jump;
	// Use this for initialization
	void Start () {        
        player = GameObject.FindGameObjectWithTag("Player");
        jump = player.GetComponent<PlayerJump>();
        performedSingleJump = false;
        performedDoubleJump = false;
    }

    void Update() {
        grounded = (player.GetComponent<Phase>().GetCurrentPhaseState()) ? false : GroundChecker.GetGroundedState();
        if (grounded) {
            performedSingleJump = false;
            performedDoubleJump = false;
            timeOffGround = 0;
        }
        else
            timeOffGround += Time.deltaTime;
    }

    public void PlayerJump() {
        float bufferJumpForSeconds = .1f;
        if (!performedSingleJump && (grounded || timeOffGround <= bufferJumpForSeconds)) {
            jump.Jump();
            performedSingleJump = true;
        }
        else if (!performedDoubleJump) {
            player.GetComponent<Rigidbody2D>().velocity = new Vector2(player.GetComponent<Rigidbody2D>().velocity.x, 0);
            jump.Jump();
            performedDoubleJump = true;
        }
    }

    public void CancelPlayerJump() {
        //if (!grounded && player.GetComponent<Rigidbody2D>().velocity.y > 0)
            jump.CancelJump();
    }
}
