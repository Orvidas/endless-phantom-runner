﻿using UnityEngine;

public class MemoryChip : MonoBehaviour {
    public int changeScoreBy = 5;
    private static Score theScore; 
	
     void Start () {
        if (theScore == null) 
            theScore = GameObject.FindGameObjectWithTag("Score").GetComponent<Score>();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Player") {
            theScore.ChangeScore(changeScoreBy);
            Destroy(gameObject);
        }
    }
}
