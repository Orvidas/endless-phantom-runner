﻿using UnityEngine;

public class PlatformController : MonoBehaviour {
    private PlatformEffector2D platform;
    private Phase playerPhaseState;
	// Use this for initialization
	void Start () {
        platform = GetComponent<PlatformEffector2D>();
        playerPhaseState = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Phase>();
	}
	
	// Update is called once per frame
	void Update () {
        if (playerPhaseState.GetCurrentPhaseState())
            platform.useOneWay = true;
        else
            platform.useOneWay = false;
	}
}
