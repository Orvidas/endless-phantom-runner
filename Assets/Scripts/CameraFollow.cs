﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    [SerializeField] private bool cameraCanPassPlayer = true;
    private bool stopCamera;
    private GameObject playerObject;
    private Transform playerTransform;
    private float cameraSpeed;

    void Start() {
        stopCamera = false;
        playerObject = GameObject.FindGameObjectWithTag("Player");
        playerTransform = playerObject.GetComponent<Transform>();
        cameraSpeed = playerObject.GetComponent<VelocityMovement>().GetMoveSpeed();

        transform.position = new Vector3(playerTransform.position.x, transform.position.y, transform.position.z);
    }

    void FixedUpdate() {
        if (!stopCamera) 
            ChooseCameraMovementMethod();
    }

    private void ChooseCameraMovementMethod() {
        if (cameraCanPassPlayer)
            CameraAdvance();
        else
            CameraFollowPlayer();
    }

    private void CameraAdvance() {
        float distanceToMove = cameraSpeed * Time.deltaTime;

        transform.position = new Vector3(transform.position.x + distanceToMove, transform.position.y, transform.position.z);
    }

    private void CameraFollowPlayer() {
        transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, transform.position.z);
    }

    public void ToggleCameraCanPassPlayer() {
        cameraCanPassPlayer = !cameraCanPassPlayer;
    }

    public bool GetCameraCanPassPlayerStatus() {
        return cameraCanPassPlayer;
    }

    public void ToggleMoveCamera() {
        stopCamera = !stopCamera;
    }

    public bool IsCameraStopped() {
        return stopCamera;
    }
}
