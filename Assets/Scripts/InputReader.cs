﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputReader : MonoBehaviour {
    #if !UNITY_IOS && !UNITY_ANDROID
    private GameObject player;
    private JumpTouchInput jumpInputScript;
    private bool jumpPressed;
    private bool jumpReleased;
    private bool phasePressed;
    private bool phaseReleased;

    void Start () {
        jumpInputScript = GetComponent<JumpTouchInput>();
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	void Update () {
        if(Input.GetButtonDown("Jump"))
            jumpPressed =  true;
        if (Input.GetButtonUp("Jump"))
            jumpReleased = true;
        if (Input.GetButtonDown("Phase"))
            phasePressed = true;
        if (Input.GetButtonUp("Phase"))
            phaseReleased = true;
	}

    void FixedUpdate() {
        if (jumpPressed)
            jumpInputScript.PlayerJump();

        if (jumpReleased)
            jumpInputScript.CancelPlayerJump();

        if (phasePressed)
            player.GetComponent<Phase>().TogglePhase();
        if (phaseReleased)
            player.GetComponent<Phase>().TogglePhase();

        jumpPressed = false;
        jumpReleased = false;
        phasePressed = false;
        phaseReleased = false;
    }
    #endif
}
