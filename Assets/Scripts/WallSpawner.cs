﻿using UnityEngine;

public class WallSpawner : MonoBehaviour, Spawner {
    public GameObject wallObject;
    public float minSpawnTime = 3f;
    public float maxSpawnTime = 6f;
    private float currentWaitTimer;
    private float endOfWaitTimer;

    public void SpawnObjects() {
        Vector3 position = new Vector3(transform.position.x, transform.position.y, wallObject.transform.localScale.y);
        Instantiate(wallObject, position, transform.rotation);
    }

    public void StartNewTimers() {
        currentWaitTimer = 0;
        endOfWaitTimer = Random.Range(minSpawnTime, maxSpawnTime);
    }

    // Use this for initialization
    void Start () {
        StartNewTimers();
	}
	
	// Update is called once per frame
	void Update () {
        currentWaitTimer += Time.deltaTime;
        if (currentWaitTimer >= endOfWaitTimer) {
            StartNewTimers();
            SpawnObjects();
        }
    }
}
