﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour {

    public void LoadTutorial() {
        SceneManager.LoadScene("TutorialScene");
    }

    public void LoadEndless() {
        SceneManager.LoadScene("TestEndlessScene");
    }
}
