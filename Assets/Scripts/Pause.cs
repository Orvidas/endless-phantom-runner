﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {
    bool paused = false;

    void Start() {
        paused = false;
    }

	public void TogglePause() {
        Time.timeScale = paused ? 1.0f : 0.0f;

        paused = !paused;
    }
}
