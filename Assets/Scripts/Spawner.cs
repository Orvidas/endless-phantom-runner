﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

interface Spawner {
    void StartNewTimers();
    void SpawnObjects();
}
