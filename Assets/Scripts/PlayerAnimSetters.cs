﻿using UnityEngine;

public class PlayerAnimSetters : MonoBehaviour {
    private GameObject player;
    private Rigidbody2D playerBody;
    private Animator playerAnimator;
    private GroundChecker groundCheck;
	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        playerBody = player.GetComponent<Rigidbody2D>();
        playerAnimator = player.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        playerAnimator.SetBool("Grounded", GroundChecker.GetGroundedState());
        playerAnimator.SetFloat("Speed", playerBody.velocity.x);
	}
}
