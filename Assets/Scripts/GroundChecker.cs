﻿using UnityEngine;

public class GroundChecker : MonoBehaviour {
    public LayerMask isGround;

    [SerializeField]
    private static bool grounded;
    private GameObject player;
	
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
        grounded = false;
	}

	void FixedUpdate () {
        grounded = Physics2D.IsTouchingLayers(player.GetComponentInChildren<CircleCollider2D>(), isGround);
    }

    public static bool GetGroundedState() {
        return grounded;
    }
}
