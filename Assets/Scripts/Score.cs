﻿using UnityEngine.UI;
using UnityEngine;

public class Score : MonoBehaviour {
    private bool increaseScore;
    private float timeScore;
    private Text textScore;
	// Use this for initialization
	void Start () {
        increaseScore = true;
        timeScore = 0f;
        textScore = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        if(increaseScore)
            timeScore += Time.deltaTime;
        DisplayScore();
	}

    private void DisplayScore() {
        textScore.text = "Score: " + Mathf.Ceil(timeScore);
    }

    public void ChangeScore(float changeBy) {
        timeScore += changeBy;
        if (timeScore < 0)
            timeScore = 0;
    }

    public void UpdateHighScore() {
        string highscore = "TempHighscore";

        if (timeScore > PlayerPrefs.GetInt(highscore))
            PlayerPrefs.SetInt(highscore, (int) Mathf.Ceil(timeScore));

        //Debug.Log("The high score is " + PlayerPrefs.GetInt(highscore));
    }

    public void SetAutoIncreaseScore(bool active) {
        increaseScore = active;
    }
}
