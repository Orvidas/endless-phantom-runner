﻿using UnityEngine;

public class FloorSpawner : MonoBehaviour, Spawner {
    public GameObject[] floorsToSpawn;
    public float minSpawnTime = .5f;
    public float maxSpawnTime = 1f;
    private float currentWaitTimer;
    private float endOfWaitTimer;
    private int numOfFloors;
    private bool waitingToSpawn;

	// Use this for initialization
	void Start () {
        numOfFloors = floorsToSpawn.Length;
        if (numOfFloors == 0)
            throw new UnassignedReferenceException();
        StartNewTimers();
        //CreateFloor();
	}
	
	// Update is called once per frame
	void Update () {
        currentWaitTimer += Time.deltaTime;
        if(currentWaitTimer >= endOfWaitTimer) {
            StartNewTimers();
            SpawnObjects();
        }
    }

    public void StartNewTimers() {
        currentWaitTimer = 0;
        endOfWaitTimer = Random.Range(minSpawnTime, maxSpawnTime);
    }

    public void SpawnObjects() {
        int newFloorIndex = Random.Range(0, numOfFloors - 1);
        GameObject newFloor = floorsToSpawn[newFloorIndex];

        Vector3 position = new Vector3(transform.position.x, transform.position.y, 0);
        Instantiate(newFloor, position, transform.rotation);
    }
}
