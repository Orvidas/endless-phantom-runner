﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour {
	public void StartNextLevel() {
        int nextLevelIndex = SceneManager.GetActiveScene().buildIndex + 1;

        if(nextLevelIndex >= 0 && nextLevelIndex < SceneManager.sceneCountInBuildSettings)
            SceneManager.LoadScene(nextLevelIndex);
        //SceneManager.LoadScene(SceneManager.GetSceneByBuildIndex(nextLevelIndex).name);
    }
}
